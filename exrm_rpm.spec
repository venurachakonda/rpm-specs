%define install_dir     /usr/local
%define _topdir         {{{PROJECT_TOPDIR}}}
%define temp_dir       /tmp
Name:    {{{PROJECT_NAME}}}
Version: {{{PROJECT_VERSION}}}

# Up issuing the release is NOT supported due to live upgrades. Please 
# up issue the Version only
Release: 0%{?dist}
Summary: {{{SUMMARY}}}

Group:         Applications
License:       GPL
URL:           https://github.com/bitwalker/exrm
Source0:       %{name}-%{version}.tar.gz
BuildRoot:     %{_tmppath}/%{name}-root
BuildArchitectures: {{{BUILD_ARCHITECTURE}}}
Packager:  Venu Rachakonda <venu.rachakonda@rackspace.com>
AutoReq:  0
Provides: %{name}
Requires: elixir = 1.3.0 , esl-erlang = 18.3-1 , nginx = 1:1.6.3-9.el7

%description
{{{DESCRIPTION}}}

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/tmp
mkdir -p $RPM_BUILD_ROOT/%{install_dir}/%{name}
cp %{SOURCE0} %{buildroot}/%{temp_dir}/

%clean
rm -rf $RPM_BUILD_ROOT

%post
if [ $1 -eq 1 ]; then
    # new install
    tar xzf %{temp_dir}/%{name}-%{version}.tar.gz -C %{install_dir}/%{name}
    rm %{temp_dir}/%{name}-%{version}.tar.gz
    /sbin/chkconfig --add %{name}
    /sbin/service %{name} start  > /dev/null 2>&1
else
    # upgrade
    mkdir %{install_dir}/%{name}/releases/%{version}
    mv %{temp_dir}/%{name}-%{version}.tar.gz %{install_dir}/%{name}/releases/%{version}/%{name}.tar.gz
    cd %{install_dir}/%{name} && bin/%{name} upgrade "%{version}"
fi
exit 0

%preun
if [ "$1" = 0 ]; then
    # package remove
    /sbin/service %{name} stop > /dev/null 2>&1
    /sbin/chkconfig --del %{name}
    rm -rf /usr/local/%{name}
fi
exit 0

%files
%defattr(-, root, root, -)
%{temp_dir}/%{name}-%{version}.tar.gz
%dir %{install_dir}/%{name}

%changelog
* Thu Jul 14 2016 Venu  0.1.0
- Build elixir RPM package 
